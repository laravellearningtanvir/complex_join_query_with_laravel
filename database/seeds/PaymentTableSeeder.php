<?php

use Illuminate\Database\Seeder;
use App\PatientTable;
use App\billTable;
class PaymentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
  public function run()
    {

    	// to create a relationship perfectly we 
    	// need the key of the other related data
    	// so we can place it insed of random  number so it can  be a perfect seeder



    	// you have to disable the foreign key constrain before 
    	// applyin gthe migration

  

    	Schema::disableForeignKeyConstraints();
    	$faker = \Faker\Factory::create();

		// first take all the id from the patient table and make an array
    	$patients = patientTable::all()->pluck('id')->toArray();

		// second take all the id from the doctor table and make an array
        $bills = billTable::all()->pluck('id')->toArray();

        DB::table('payment_tables')->delete();
        for($i=0;$i<=100;$i++){

        	// take a random doctor and patient from the list
        	$patient_id = 	$faker->randomElement($patients);
 			$bill_id = 	$faker->randomElement($bills);
        	DB::table('payment_tables')->insert([
                "Pay_recipt_number" => rand(10,100),
                "Pay_method" => "Bkash",
                "Pay_amount" => 500,
                "created_at" => \Carbon\Carbon::now(),
                "updated_at" => \Carbon\Carbon::now(),
                "makes" => $patient_id,
                "pays" =>$bill_id
            ]);

        }
    }
}

