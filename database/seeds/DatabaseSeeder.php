<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        //$this->call(UsersTableSeeder::class);

    	// you need to first seed some table that is not related with other
    	// other wise you need to use the random and the perfect seeder wont work


        $this->call(DoctorTableSeeder::class);
        $this->call(PatientTableSeeder::class);
        $this->call(AppointmentTableSeeder::class);
        $this->call(BillTableSeeder::class);
        $this->call(PaymentTableSeeder::class);
    }
}
