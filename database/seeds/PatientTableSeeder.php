<?php

use Illuminate\Database\Seeder;
use App\patientTable;
class PatientTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
   public function run()
    {
        // add the faker for creating the seeder
        // Truncate the table 
        //doctorTable::truncate();
        
        $faker = \Faker\Factory::create();

        // loop through the 
        for($i=0;$i<=100;$i++){
            // create the instance
            patientTable::create([
                //"App_reason" => Str::random(60),
                "pat_first_name" => $faker->firstName,
                "pat_last_name" => $faker->lastName,
                "pat_address" => Str::random(30),
                "pat_city" => Str::random(5),
                "pat_state" => Str::random(5),
                //"scheduled_for" => rand(1,100),
                //"scheduled" => rand(1,100),
                //"App_reason" => Str::random(60),
                "created_at" => \Carbon\Carbon::now(),
                "updated_at" => \Carbon\Carbon::now(),
            ]);
        }
    }
}