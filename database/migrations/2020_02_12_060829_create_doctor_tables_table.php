<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDoctorTablesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('doctor_tables', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('Doc_mobile');
            $table->string('DOC_firstname');
            $table->string('DOC_lastname');
            $table->string('DOC_dept');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('doctor_tables');
    }
}
