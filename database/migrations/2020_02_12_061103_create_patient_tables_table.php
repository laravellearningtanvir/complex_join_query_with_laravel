<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePatientTablesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('patient_tables', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('pat_first_name');
            $table->string('pat_last_name');
            $table->string('pat_address');
            $table->string('pat_city');
            $table->string('pat_state');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('patient_tables');
    }
}
